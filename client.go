package main

import (
	"github.com/gorilla/websocket"
	"log"
)

type Client struct {
	connection *websocket.Conn
	out        chan map[string]interface{}
	keep_running bool
	clientClose chan *Client
	in  chan map[string]interface{}
}

func NewClient(connection *websocket.Conn, clientClose chan *Client, in chan map[string]interface{}) *Client{
	return &Client{
		connection: connection,
		out: make(chan map[string]interface{}),
		clientClose: clientClose,
		in: in,
		keep_running: true,
	}
}

func (c *Client) Close() {
	c.clientClose <- c
	c.keep_running = false
	close(c.out)
}

func (c *Client) writer() {
	defer func() {
		c.connection.Close()
		recover()
	}()
	for c.keep_running {
		select {
		case message, ok := <-c.out:
			if !ok {
				c.connection.WriteMessage(websocket.CloseMessage, []byte{})
				c.Close()
				return
			}
			c.connection.WriteJSON(message)
		}
	}
}

func (c *Client) reader() {
	defer func() {
		c.clientClose <- c
		c.connection.Close()
		recover()
	}()
	for c.keep_running {
		message := make(map[string]interface{})
		err := c.connection.ReadJSON(&message)
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				log.Printf("error: %v", err)
				break
			}else{
				log.Printf("read error: %v", err)
				c.Close()
			}
		}
		c.in <- message
	}
}
