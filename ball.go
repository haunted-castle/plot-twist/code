package main

import (
	"math"
	"time"
)

type Ball struct {
	is_locked bool
	x0 int
	y0 int
	t0 int64
}

func NewBall() *Ball {
	return &Ball{x0: 50, y0: 50, t0: 0, is_locked: true}
}
func (ball *Ball) Move(x,y int) {
	t := time.Now().UnixNano()
	dx := x - ball.x0
	dy := y - ball.y0
	dt := t - ball.t0

	if dt > 1000000 { dt = 1000000 }
	d := math.Sqrt(float64(dx*dx + dy*dy))
	v := 1000000 * d/float64(dt)
	s := 1.0
	if v > 10.0 { s = 10.0/v }

	x1 := ball.x0 + int(s*float64(dx))
	y1 := ball.y0 + int(s*float64(dy))

	if x1 > 100 { x1 = 100 }
	if x1 < 0 { x1 = 0 }
	if y1 > 100 { y1 = 100}
	if y1 < -100 { y1 = -100 }
	if y1 < 0 && ball.is_locked {
		if !(x1 >= 30 && x1 <= 70 && y1 >= -70 && y1 <= -30) {
			y1 = 0
		}
	}
	ball.x0 = x1
	ball.y0 = y1
	ball.t0 = t


}
