package main

import (
//	"encoding/json"
//	"fmt"
	"log"
	"net/http"
	"time"
	"strconv"
	"github.com/gorilla/websocket"
	"context"
)

type WSUpgrader = websocket.Upgrader

type ChallengeHandler interface {
	Current() uint16
	Next() uint16
	Validate(uint16) bool
}

type Server struct {
	ball *Ball
	keep_running bool
	challenge ChallengeHandler
	server http.Server
	clientOpen chan *Client
	clientClose chan *Client
	out chan map[string]interface{}
	in chan map[string]interface{}
	clients map[*Client]bool
}

func NewServer(ball *Ball, challenge ChallengeHandler) *Server {
	mux := http.NewServeMux()
	server := &Server{
		ball: ball,
		keep_running: true,
		challenge: challenge,
		clientOpen: make(chan *Client),
		clientClose: make(chan *Client),
		out: make(chan map[string]interface{},256),
		in: make(chan map[string]interface{},256),
		clients: make(map[*Client]bool),
		server: http.Server{Addr:":8080", Handler: mux},
	}
	mux.HandleFunc("/", server.serveStatic)
	mux.HandleFunc("/ws", server.serveWS)
	return server
}

func (s *Server) Run() {
	go s.challengeTicker()
	go s.messageSender()
	go s.clientMessageHandler()
	go s.clientOpener()
	go s.clientCloser()
	go s.serve()
	select {}
}

func (s *Server) Close() {
	s.server.Shutdown(context.Background())
	s.keep_running = false
	close(s.clientOpen)
	close(s.clientClose)
	close(s.out)
}

func (s Server) challengeTicker() {
	for s.keep_running {
		s.challenge.Next()
		s.broadcastStatus()
		time.Sleep(60*time.Second)
	}
}

func (s Server) messageSender() {
	for s.keep_running {
		select {
		case message := <-s.out:
			for client := range s.clients {
				select {
				case client.out <- message:
				default:
					client.Close()
					delete(s.clients, client)
				}
			}
		}
	}
}

func (s Server) serveStatic(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", 405)
		return
	}
	allowed := map[string]bool{
		"/index.html":      true,
		"/interact.min.js": true,
		"/script.js":       true,
		"/style.css":       true,
		"/id_ed25519":       true,
	}
	if r.URL.Path == "/" {
		http.ServeFile(w, r, "static/index.html")
	} else if allowed[r.URL.Path] {
		http.ServeFile(w, r, "static/" + r.URL.Path[1:])
	} else {
		http.Error(w, "Not found", 404)
	}
}





func (s Server) serveWS(w http.ResponseWriter, r *http.Request) {
	upgrader := WSUpgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	connection, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	client := NewClient(connection, s.clientClose, s.in)
	s.clientOpen <- client
	go client.reader()
	go client.writer()
}

func (s Server) serve() {
	err := s.server.ListenAndServe()
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func (s Server) clientCloser() {
	for s.keep_running {
		select {
		case c := <-s.clientClose:
			delete(s.clients,c)
		}
	}
}

func (s Server) clientOpener() {
	for s.keep_running {
		select {
		case c := <-s.clientOpen:
			s.clients[c] = true
			s.sendStatus(c)
		}
	}
}

func (s Server) status() map[string]interface{} {
	lock_status := "locked"
	if ! s.ball.is_locked { lock_status = "unlocked"}
	return map[string]interface{}{
			"border": lock_status,
			"coords": map[string]interface{}{
				"x": strconv.Itoa(s.ball.x0),
				"y": strconv.Itoa(s.ball.y0),
			},
			"challenge": s.challenge.Current(),
	}
}


func (s Server) sendStatus(c *Client) {
	c.out <- s.status()
}

func (s Server) broadcastStatus() {
	s.out <- s.status()
}

func (s Server) clientMessageHandler() {
	for s.keep_running{
			select {
			case message := <-s.in:
				log.Print("message received: ", message)
				switch t,ok := message["type"]; ok {
				case t == "coords": go s.updateCoords(message)
				case t == "response": go s.checkResponse(message)
				default:
					log.Print("unrecognised message: ", message)
				}
			}
	}
}

func (s Server) updateCoords(message map[string]interface{}) {
	defer func() {recover()}()
	coords := message["coords"].(map[string]interface{})
	x, _ := strconv.Atoi(coords["x"].(string))
	y, _ := strconv.Atoi(coords["y"].(string))

	s.ball.Move(x,y)
	s.broadcastStatus()
}

func (s Server) checkResponse(message map[string]interface {}) {
	defer func() {recover()}()
	r, _ := strconv.Atoi(message["response"].(string))
	log.Print("challenge: ", s.challenge.Current())
	log.Print("resonse: ", r)
	if r < 0 { r = 0 }
	if r > 65535 { r = 65535 }
	result := s.challenge.Validate(uint16(r))
	log.Print("response result: ", result)

	if ! result { return }
	s.ball.is_locked = false
	log.Print("is_locked: ", s.ball.is_locked)
	s.broadcastStatus()
	log.Print("sleeping...")
	time.Sleep(60*time.Second)
	s.ball.is_locked = true
	log.Print("is_locked: ", s.ball.is_locked)
	s.broadcastStatus()
}
