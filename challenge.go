package main

import (
	"time"
)

type Challenge struct {
	last time.Time
	current uint16
	gf16 GF16
}

func NewChallenge() *Challenge {
	gf16 := NewGF16()
	return &Challenge{current: gf16.Rand(), gf16: gf16}
}

func (c Challenge) Current() uint16 {
	return c.current
}

func (c *Challenge) Next() uint16 {
	c.current = c.gf16.Mul(c.current, c.gf16.Rand())
	return c.current
}

func (c Challenge) Validate(r uint16) bool {
	if time.Since(c.last) < time.Second {
		return false
	}
	c.last = time.Now()
	return c.gf16.Mul(c.current, r) == 1
}
