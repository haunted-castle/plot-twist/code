package main




func main() {
	challenge := NewChallenge()
	ball := NewBall()
	server := NewServer(ball, challenge)

	server.Run()

	// go challengeTicker()
	// go messageSender()
	// go clientMessageHandler()
	// go clientOpener()
	// go clientCloser()
	//
	// http.HandleFunc("/", serveStatic)
	// http.HandleFunc("/ws", serveWS)
	//
	//
	// go func() {
	// 	err := http.ListenAndServe(":8080", nil)
	// 	if err != nil {
	// 		log.Fatal("ListenAndServe: ", err)
	// 	}
	// }()
	//
	// select{}
	// keep_running = false
}
