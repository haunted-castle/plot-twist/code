package main

import (
	"math/rand"
	"time"
)

const FIELD_BASE uint = 0x1100b


type GF16 struct {
	rng *rand.Rand
	w uint
	log []uint16
	ilog []uint16
}

func NewGF16() GF16 {
	ret := GF16{}
	ret.w = 1 << 16
	ret.log = make([]uint16, ret.w)
	ret.ilog = make([]uint16, ret.w)
	for b,l := uint(1),uint(0); l < ret.w; l++ {
		ret.log[b] = uint16(l)
		ret.ilog[l] = uint16(b)
		b = b << 1
		if b & ret.w != 0 {
			b = b ^ FIELD_BASE
		}
	}
	return ret
}

func (f GF16) Mul(a uint16, b uint16) uint16 {
	if a == 0 || b == 0 { return 0 }
	s := int(f.log[a]) + int(f.log[b])
	if s >= int(f.w) { s -= int(f.w) }
	return f.ilog[s]
}

func (f *GF16) Rand() uint16 {
	if f.rng == nil {
		f.rng = rand.New(rand.NewSource(time.Now().UnixNano()))
	}
	return uint16(1 + f.rng.Int31n(int32(f.w)-1))
}

func (f GF16) W() uint16 {
	return uint16(f.w)
}
