


document.addEventListener('DOMContentLoaded', function () {
	'use strict';

	var ball = document.getElementById("ball");
	var court = document.getElementById("court");
	var challenge = document.getElementById("challenge");
	var response = document.getElementById("response");
	var border = document.getElementById("border");

	var court2screen = court.getScreenCTM();
	var screen2court = court2screen.inverse();
	var ws;

	function makeSocket() {
		ws = new WebSocket(location.href.replace("http","ws") + "/ws")
		ws.onmessage = function (event) {
	//  	console.log(event.data);
			var data = JSON.parse(event.data);
			ball.setAttribute( "transform","translate(" + data['coords']['x'] + "," + data['coords']['y'] +")");
			challenge.innerText = data['challenge']
			border.className.baseVal = data['border']
		}
		ws.onclose = function(event) {
			makeSocket()
		}

	}

	makeSocket()

	response.onkeypress = function runScript(e) {
    if (e.keyCode == 13) {
			var payload = {
				"type": "response",
				"response": response.value
			}
//			console.log(payload)
			ws.send(JSON.stringify(payload))
    }
	}
	var x0,y0

	interact("#ball")
		.draggable({
			inertia: true,
			onstart: function(e){
				var x0 = e.x0 * screen2court.a + e.y0 * screen2court.c + screen2court.e;
				var y0 = e.x0 * screen2court.b + e.y0 * screen2court.d + screen2court.f;

			},
			onmove: function(e){

				var x = e.clientX * screen2court.a + e.clientY * screen2court.c + screen2court.e;
				var y = e.clientX * screen2court.b + e.clientY * screen2court.d + screen2court.f;


				var dx = x - x0;
				var dy = y - y0;
				if( ! e.dt > 0) { e.dt = 1; }
				var d = Math.sqrt(dx*dx + dy*dy);
				var v = d/e.dt;
				if( v > .1) {
					x = x0 + (dx*.1/v);
					y = y0 + (dy*.1/v);
				}
				var payload = {
					"type": "coords",
					"coords": {
						"x": String(x.toFixed(0)),
						"y": String(y.toFixed(0))
					}
				}
//				console.log(payload)
				if( ws.readyState == ws.OPEN ){
					ws.send(JSON.stringify(payload))
				}
			},

			restrict: { restriction: court }
		})



	document.addEventListener('dragstart', function (event) {
		event.preventDefault();
	});

});
